from src.detector.detector import Detector
from src.detector.utils.image_utils import align_image
from src.config import corner_detection
from src.RoiCrop.image_constant_roi import CMTND
import easyocr
import cv2
import pytesseract

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


def recognize(image):
    field_dict = dict()

    def cropImageRoi(roi):
        roi_cropped = image[int(roi[1]): int(roi[1] + roi[3]), int(roi[0]): int(roi[0] + roi[2])]
        return roi_cropped

    def extractDataWithEasyOCR():
        # display_img()
        # cv2.imwrite("cmtnd.jpg", image)
        list_ans = list()
        reader = easyocr.Reader(['vi'])
        for key, roi in CMTND.ROIS.items():
            for r in roi:
                crop_img = cropImageRoi(r)
                gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
                gray = cv2.multiply(gray, 1.55)
                output = reader.readtext(gray)
                print(output)
                list_ans.append(output)
        return list_ans

    def extractDataWithTesseractOCR():
        list_ans = list()
        for key, roi in CMTND.ROIS.items():
            for r in roi:
                crop_img = cropImageRoi(r)
                gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
                gray = cv2.multiply(gray, 1.8)
                output = pytesseract.image_to_string(gray, lang="vie")
                list_ans.append(output)


        return list_ans

    def response_cmtnd(dataCmtnd):
        text = ''
        if len(dataCmtnd) == 1:
            text = text + dataCmtnd[0][1]
        else:
            for d in dataCmtnd:
                text = text + ' ' + d[1]

        return text

    data = extractDataWithEasyOCR()
    # data2 = extractDataWithTesseractOCR()
    # print(data2)
    field_dict['id'] = response_cmtnd(data[0]).replace(" ", "")
    field_dict['name'] = response_cmtnd(data[1])
    field_dict['birth_date'] = response_cmtnd(data[2])
    field_dict['address'] = response_cmtnd(data[3]) + ' ' + response_cmtnd(data[4])
    field_dict['city'] = response_cmtnd(data[5]) + ' ' + response_cmtnd(data[6])

    return field_dict


class CompletedModel(object):
    def __init__(self):
        self.corner_detection_model = Detector(path_to_model=corner_detection['path_to_model'],
                                               path_to_labels=corner_detection['path_to_labels'],
                                               nms_threshold=corner_detection['nms_ths'],
                                               score_threshold=corner_detection['score_ths'])

    def detect_corner(self, image):
        detection_boxes, detection_classes, category_index = self.corner_detection_model.predict(image)

        coordinate_dict = dict()
        height, width, _ = image.shape

        for i in range(len(detection_classes)):
            label = str(category_index[detection_classes[i]]['name'])
            real_ymin = int(max(1, detection_boxes[i][0]))
            real_xmin = int(max(1, detection_boxes[i][1]))
            real_ymax = int(min(height, detection_boxes[i][2]))
            real_xmax = int(min(width, detection_boxes[i][3]))
            coordinate_dict[label] = (real_xmin, real_ymin, real_xmax, real_ymax)

        # align image
        cropped_img = align_image(image, coordinate_dict)

        return cropped_img

    def predict(self, image):
        cropped_image = self.detect_corner(image)
        return recognize(cropped_image)
