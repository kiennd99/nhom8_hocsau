corner_detection = {
    'path_to_model': './src/detector/config_corner_detection/model.tflite',
    'path_to_labels': './src/detector/config_corner_detection/label_map.pbtxt',
    'nms_ths': 0.2,
    'score_ths': 0.3
}
