import uvicorn
from fastapi import FastAPI, File, UploadFile
from starlette.responses import RedirectResponse
from fastapi.openapi.utils import get_openapi
from serve.serve_model import *

app_desc = """<h3>Nhom 8</h3>"""

app = FastAPI(title="Trích xuất thông tin", description=app_desc)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Trích Xuất Thông Tin",
        version="",
        description="Trích xuất thông tin từ chứng minh thư nhân dân",
        routes=app.routes,

    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


@app.get("/", include_in_schema=False)
async def index():
    return RedirectResponse(url="/docs")


@app.post("/cmtnd/image")
async def Upload_CMTND(file: UploadFile = File(...)):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        return "File không đúng định dạng. Vui lòng thử lại!"

    image = read_image_file(await file.read())
    prediction = predict(image)

    return prediction


if __name__ == "__main__":
    uvicorn.run(app)
